package HW_12_2_fix;

public class MyRunnable implements Runnable{
    @Override
    public void run() {
        try {
            System.out.println("Started: "+Thread.currentThread().getId());
            Thread.sleep(3000);
            System.out.println("Finished: "+Thread.currentThread().getId());
        }catch (Exception e){
            System.out.println();
        }
    }
}
