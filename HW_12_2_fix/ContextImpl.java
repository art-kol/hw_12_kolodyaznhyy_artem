package HW_12_2_fix;

public class ContextImpl implements Context{

    @Override
    public int getCompletedTaskCount() {
        return completeTask.get();
    }

    @Override
    public int getFailedTaskCount() {
        return failTask.get();
    }

    @Override
    public int getInterruptedTaskCount() {
        return interruptTask.get();
    }

    @Override
    public void interrupt() {
        isInterrupted.set(true);
    }

    @Override
    public boolean isFinished() {
        return ex.isShutdown();
    }
}
