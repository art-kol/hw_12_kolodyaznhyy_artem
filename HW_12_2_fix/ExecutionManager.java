package HW_12_2_fix;

public interface ExecutionManager {
    Context execute(Runnable callback, Runnable... tasks) throws InterruptedException;
}
