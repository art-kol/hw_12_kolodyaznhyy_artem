package HW_12_2_fix;

public class Main {

    public static void main(String[] args) throws Exception {

        ExecutionManagerImpl ex = new ExecutionManagerImpl();

        Runnable[] runnables = new Runnable[20];
        for (int i = 0; i < runnables.length; i++) {
            runnables[i] = new MyRunnable();
        }
        Context cont = ex.execute(new CallBack(), runnables);

        Thread.sleep(3100);
        System.out.println("Количество выполненный заданий: "+cont.getCompletedTaskCount());

        cont.interrupt();
        Thread.sleep(3100);
        System.out.println("Количество прерванных заданий: "+cont.getInterruptedTaskCount());

        Thread.sleep(5000);
        System.out.println("Проверка состояния пула потоков: "+cont.isFinished());

    }
}
