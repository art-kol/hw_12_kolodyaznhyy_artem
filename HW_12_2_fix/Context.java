package HW_12_2_fix;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public interface Context {

    AtomicInteger completeTask = new AtomicInteger(0);
    AtomicInteger interruptTask = new AtomicInteger(0);
    AtomicInteger countTask = new AtomicInteger();
    AtomicBoolean isInterrupted = new AtomicBoolean(false);
    AtomicInteger failTask = new AtomicInteger(0);
    ExecutorService ex = Executors.newFixedThreadPool(5);

    int getCompletedTaskCount();

    int getFailedTaskCount();

    int getInterruptedTaskCount();

    void interrupt();

    boolean isFinished();

}
