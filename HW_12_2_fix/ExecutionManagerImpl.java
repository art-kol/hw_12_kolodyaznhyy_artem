package HW_12_2_fix;

public class ExecutionManagerImpl implements ExecutionManager {

    @Override
    public Context execute(Runnable callback, Runnable[] tasks){

        Context context = new ContextImpl();

        for(Runnable task: tasks){
            Runnable runnable1 = () ->{
                if(!context.isInterrupted.get()) {
                    try {
                        task.run();
                        context.completeTask.getAndIncrement();
                    } catch (RuntimeException e) {
                        context.failTask.getAndIncrement();
                    }
                }else {
                    context.interruptTask.getAndIncrement();
                }
                if(context.countTask.incrementAndGet() == tasks.length) {

                    context.ex.shutdown();
                    if(context.ex.isShutdown()){
                        callback.run();
                    }
                }
            };
            if(!context.isInterrupted.get()){
                context.ex.submit(runnable1);
            }
        }
        return context;
    }
}



